require_relative 'operations'

module Calculator
  class Menu
    def initialize

    puts "╔═══════════════════════╗\n| Welcome to Calculator |\n╚═══════════════════════╝"
    puts "1. Biased mean"
    puts "2. No integers calculator"
    puts "3. Filter films"
    puts "4. Quit"
    puts "Choose an option: "
    optionMenu = gets.chomp.to_i
    
   
    case optionMenu

      when 1
        puts "Enter a JSON string with names and grades:"
        grades = gets.chomp
        puts "Enter a blacklist:"
        blacklist = gets.chomp
        grade = Operations.new
        puts "Average: #{grade.biased_mean(grades,blacklist)}"

      when 2
        puts "Enter a number (string format):"
        numbers = gets.chomp.to_s
        number = Operations.new
        puts number.no_integers(numbers)  

      when 3
        puts "Enter a gender:"
        genres = gets.chomp
        puts "Enter a year:"
        year = gets.chomp
        movies = Operations.new
        puts movies.filter_films(genres,year)

      when 4
        exit

      else
        puts "Invalid operation. Please try again:"

      end
    end
  end
end

