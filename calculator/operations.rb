require_relative '../extra_operations'
require 'net/http'
require 'json'

module Calculator
  class Operations
    include ExtraOperations
  
    def biased_mean(grades, blacklist)
      grade = JSON.parse(grades)

        aux = 0
        aux2 = 0
        average = 0
        count = 0
        while aux < grade.length
          while aux2 < blacklist.split.length
            if grade.keys[aux] == blacklist.split[aux2]
              grade.delete(blacklist.split[aux2])
            end
            aux2+=1
          end
          aux+=1
          aux2=0
        end

        aux=0
        while aux < grade.length
          average += grade.values[i]
          count += 1
          aux += 1
        end
        result = average/count
    end
  
    def no_integers(numbers)
      
      aux=0
      outcome = []
      number = numbers.split

      while aux < number.length
        if number[aux][number[aux].length-2] == '0'

          if number[aux][number[aux].length-3] == '5' || number[aux][number[aux].length-3] == '0'

            outcome[aux] = 'S '

          else

            outcome[aux] = 'N '

          end

        elsif number[aux][number[aux].length-2] == '5'

          if number[aux][number[aux].length-3] == '2' || number[aux][number[aux].length-3] == '7'

            outcome[aux] = 'S '

          else

            outcome[aux] = 'N '

          end

        else

          outcome[aux] = 'N '

        end

        aux+=1
      end

      aux=0
      numbers = ""

      while aux<outcome.length

       numbers << outcome[aux]
       aux+=1

      end

      numbers
      
    end
  
    def filter_films(genres, year)
      films = get_films
      movies = films[:movies]      
      
      aux=0;
      while aux<movies.length

        if movies[aux][:genres].include?(genres)

          if movies[aux][:year] >= year

            puts movies[aux][:title]

          end

        end

      aux+=1
      
      end
    end
    
    private
  
    def get_films
      url = 'https://raw.githubusercountent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end
  end
end